package net.mcbeelen.projecteuler.p002;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class EvenFibonacciNumberTest {

	private EvenFibonacciNumbers evenFibonacciNumbers = new EvenFibonacciNumbers();

	@Test
	public void itShouldSumTwoForMaxTwo() throws Exception {
		assertThat(evenFibonacciNumbers.calculateSumUpTo(2), is(2));
	}

	@Test
	public void itShouldSumTen() throws Exception {
		assertThat(evenFibonacciNumbers.calculateSumUpTo(10), is(10));
	}

	@Test
	public void itShouldSumForty() throws Exception {
		assertThat(evenFibonacciNumbers.calculateSumUpTo(40), is(44));
	}
}
