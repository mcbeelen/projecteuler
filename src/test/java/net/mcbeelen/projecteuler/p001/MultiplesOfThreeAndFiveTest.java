package net.mcbeelen.projecteuler.p001;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MultiplesOfThreeAndFiveTest {

	@Test
	public void itShouldReturn23For10() throws Exception {
		MultiplesOfThreeAndFive multiplesOfThreeAndFive = new MultiplesOfThreeAndFive();
		final Integer sumOfMultiplesOfThreeAndFive = multiplesOfThreeAndFive.sumOfMultiplesOfThreeAndFive(10);

		assertThat(sumOfMultiplesOfThreeAndFive, is(23));
	}
}