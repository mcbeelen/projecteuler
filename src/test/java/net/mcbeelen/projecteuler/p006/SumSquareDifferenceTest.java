package net.mcbeelen.projecteuler.p006;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

public class SumSquareDifferenceTest {

	@Test
	public void itShouldCalculate2640For10() throws Exception {

		assertThat(SumSquareDifference.calculate(10), closeTo(new BigDecimal("2640.0"), new BigDecimal("0.5")));

	}
}
