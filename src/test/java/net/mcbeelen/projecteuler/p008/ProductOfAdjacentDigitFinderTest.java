package net.mcbeelen.projecteuler.p008;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ProductOfAdjacentDigitFinderTest {

	@Test
	public void itShouldFind5832For4Digits() throws Exception {

		ProductOfAdjacentDigitFinder finder = new ProductOfAdjacentDigitFinder();

		assertThat(finder.find(4), is(5832));


	}
}
