package net.mcbeelen.projecteuler.p003;

import org.junit.Test;

import java.math.BigDecimal;

import static net.mcbeelen.projecteuler.p003.LargestPrimeFactorFinder.findLargestPrimeFactor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LargestPrimeFactorFinderTest {

	@Test
	public void itShouldFindFiveAsLargestPrimeOf15() throws Exception {
		assertThat(findLargestPrimeFactor(new BigDecimal("15")), is(new BigDecimal("5")));
	}

	@Test
	public void itShouldFind11AsLargestPrimeOf33() throws Exception {
		assertThat(findLargestPrimeFactor(new BigDecimal("33")), is(new BigDecimal("11")));
	}

	@Test
	public void itShouldFind11AsLargestPrimeOf231() throws Exception {
		assertThat(findLargestPrimeFactor(new BigDecimal("231")), is(new BigDecimal("11")));
	}
}
