package net.mcbeelen.projecteuler.p004;

import org.junit.Test;

import static net.mcbeelen.projecteuler.p004.PalindroomProductFinder.isPalindroom;
import static org.junit.Assert.*;

public class PalindroomProductFinderTest {

	@Test
	public void appleIsFalse() throws Exception {
		assertFalse(isPalindroom("apple"));
	}

	@Test
	public void anmnaIsFalse() throws Exception {
		assertTrue(isPalindroom("anmna"));
	}

	@Test
	public void ankknaIsFalse() throws Exception {
		assertTrue(isPalindroom("ankkna"));
	}

}