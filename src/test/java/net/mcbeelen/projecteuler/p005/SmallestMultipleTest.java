package net.mcbeelen.projecteuler.p005;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SmallestMultipleTest {

	@Test
	public void itShouldFind2520ForNumbersUpTo10() throws Exception {

		SmallestMultiple smallestMultiple = new SmallestMultiple();

		assertThat(smallestMultiple.findSmallestMultipleOfAllNumbersUpTo(10), is(2520));

	}
}
