package net.mcbeelen.projecteuler.p006;

import java.math.BigDecimal;
import java.util.stream.IntStream;

public class SumSquareDifference {
	public static BigDecimal calculate(int n) {

		final int sum = IntStream.rangeClosed(1, n).sum();
		final BigDecimal squaresSummed = new BigDecimal(IntStream.rangeClosed(1, n)
				.asDoubleStream()
				.map(value -> Math.pow(value, 2))
				.sum());

		final BigDecimal sumSquared = new BigDecimal(Math.pow(sum, 2));
		final BigDecimal subtract = sumSquared.subtract(squaresSummed);
		System.out.println(sumSquared + " - " + squaresSummed + " = " + subtract);

		return subtract;

	}


	public static void main(String[] args) {
		System.out.println("100 --> " + calculate(100));
	}
}
