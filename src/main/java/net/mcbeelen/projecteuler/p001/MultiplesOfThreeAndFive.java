package net.mcbeelen.projecteuler.p001;

import java.util.stream.IntStream;

public class MultiplesOfThreeAndFive {

	public Integer sumOfMultiplesOfThreeAndFive(int n) {

		Integer sum = IntStream.range(1, n)
				.filter(i -> (i % 3 == 0) || (i % 5 == 0))
				.sum();

		return sum;
	}

	public static void main(String[] args) {
		MultiplesOfThreeAndFive multiplesOfThreeAndFive = new MultiplesOfThreeAndFive();
		System.out.println(multiplesOfThreeAndFive.sumOfMultiplesOfThreeAndFive(1000));
	}
}
