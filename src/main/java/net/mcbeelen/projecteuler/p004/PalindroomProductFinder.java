package net.mcbeelen.projecteuler.p004;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PalindroomProductFinder {


	public static void main(String[] args) {

		List<Integer> palindroomProducts  = new ArrayList<>();

		for (int i = 100; i <= 999 ; i++) {
			for (int j = 100; j <= 999 ; j++) {
				int product = i * j;

				if(isPalindroom(Integer.toString(product))) {
					System.out.println("Palindroom Product found for " + i + " * " + j + " = " + product);
					palindroomProducts.add(product);
				}
			}
		}

		Collections.sort(palindroomProducts);

		System.out.println(palindroomProducts.get(palindroomProducts.size() - 1));
	}

	protected static boolean isPalindroom(String text) {

		StringBuilder reverser  = new StringBuilder();
		reverser.append(text);
		final String reverse = reverser.reverse().toString();
		return text.equals(reverse);
	}

}
