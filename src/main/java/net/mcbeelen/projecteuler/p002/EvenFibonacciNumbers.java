package net.mcbeelen.projecteuler.p002;

import java.util.Stack;

public class EvenFibonacciNumbers {

	private Stack<Integer> fibonacciNumbers = new Stack<>();


	public EvenFibonacciNumbers() {
		fibonacciNumbers.push(1);
		fibonacciNumbers.push(2);
	}

	public Integer calculateSumUpTo(int n) {

		int nextFibonacci = getNextFibonacci();

		while (nextFibonacci < n) {
			fibonacciNumbers.push(nextFibonacci);
			nextFibonacci = getNextFibonacci();
		}

		final Integer sum = fibonacciNumbers.stream()
				.filter(integer -> integer % 2 == 0)
				.mapToInt(Integer::intValue)
				.sum();


		return sum;
	}

	private int getNextFibonacci() {
		return fibonacciNumbers.get(fibonacciNumbers.size() - 2) + fibonacciNumbers.get(fibonacciNumbers.size() - 1);
	}

	public static void main(String[] args) {
		EvenFibonacciNumbers evenFibonacciNumbers = new EvenFibonacciNumbers();
		final Integer sumUpTo = evenFibonacciNumbers.calculateSumUpTo(4000000);
		System.out.println(sumUpTo);
	}


}
