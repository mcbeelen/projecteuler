package net.mcbeelen.projecteuler.p003;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.Matchers.closeTo;

public class LargestPrimeFactorFinder {

	public static BigDecimal findLargestPrimeFactor(BigDecimal bigNumber) {

		System.out.println("Finding factors for: " + bigNumber);

		Stack<BigDecimal> factors = new Stack();
		BigDecimal candidate = new BigDecimal(3);

		while (bigNumber.compareTo(candidate) >= 0) {

			final BigDecimal remainder = bigNumber.remainder(candidate);

			if(closeTo(new BigDecimal(0), new BigDecimal(0.5)).matches(remainder)) {
				bigNumber = bigNumber.divide(candidate, BigDecimal.ROUND_DOWN);
				factors.push(candidate);
				System.out.println("  Found factor: " + candidate);
			} else {
				candidate = candidate.add(new BigDecimal(2));
			}
		}

		return factors.peek();


	}


	public static void main(String[] args) {
		System.out.println("Largest Prime Factor of 600851475143 = " + findLargestPrimeFactor(new BigDecimal("600851475143")));

	}
}
