package net.mcbeelen.projecteuler.p005;

import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

import java.util.concurrent.TimeUnit;

public class SmallestMultiple {
	public Integer findSmallestMultipleOfAllNumbersUpTo(int n) {

		Multiset<Integer> factorsForSmallestInteger = HashMultiset.create();

		for (int i = 2; i < n; i++) {
			Multiset<Integer> factorsOfI = findFactorsOf(i);
			Multisets.removeOccurrences(factorsOfI, factorsForSmallestInteger);
			for (Integer integer : factorsOfI) {
				factorsForSmallestInteger.setCount(integer, factorsForSmallestInteger.count(integer) + factorsOfI.count(integer));
			}
		}
		Integer product = 1;
		for (Integer factor : factorsForSmallestInteger) {
			product *= factor;
		}

		return product;
	}

	private Multiset<Integer> findFactorsOf(int n) {
		final HashMultiset<Integer> factors = HashMultiset.create();

		int evenCandidate = 2;
		int oddCandidate = 3;

		while (n > 1 && n % evenCandidate == 0) {
			n /= evenCandidate;
			factors.add(evenCandidate);
		}
		while (n > 1) {
			if (n % oddCandidate == 0) {
				n /= oddCandidate;
				factors.add(oddCandidate);
			} else {
				oddCandidate += 2;
			}
		}
		return factors;
	}

	public static void main(String[] args) {
		SmallestMultiple smallestMultiple = new SmallestMultiple();
		Stopwatch stopwatch = Stopwatch.createStarted();
		final Integer smallestMultipleOf20 = smallestMultiple.findSmallestMultipleOfAllNumbersUpTo(10000);
		stopwatch.stop();
		System.out.println("Found " + smallestMultipleOf20 + "  in " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " " +  TimeUnit.MILLISECONDS);
	}

}
